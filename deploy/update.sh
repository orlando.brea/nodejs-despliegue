#!/bin/sh


set -e


#pm2 stop index
#pm2 delete index

#rm -rf /home/ec2-user/app/

git clone https://gitlab.com/orlando.brea/nodejs-despliegue.git app

cd /home/ec2-user/app
mv env_server .env


#install npm packages
echo "Iniciando app - curso nodejs"
npm install

pm2 start index.js