#!/bin/sh


set -e

apk --update add openssh-client

eval $(ssh-agent -s)
echo "$SSH_KEY" | tr -d '\r' | ssh-add - > /dev/null
# SSH_KEY es obtenida de la configuracion de gitlab (variables)


./deploy/disableHostKeyChecking.sh



echo "Deplegando en Servidor..."
ssh ec2-user@34.227.221.222 'bash' < ./deploy/update.sh

